#!/bin/bash

#Define function picking two variables
Hello(){
    echo "bado naendelea $1 $2"
}

#invoke function
Hello Kuendelea proper

#capture value returned previously
ret=$?
echo "Return value is $ret"

## Nesting functions
number_one= () {
    echo "Alpha online .... over"
    number_two
}

number_two () {
    echo "Beta online .. over"
}

number_one