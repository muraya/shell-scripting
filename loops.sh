#!/bin/sh

#For loop
for var in 0 1 2 3 4 5
do
    echo $var
done;


#While loop (while a is lesser than lt 10)
a=0
while [ $a -lt 10 ]
do 
#print a then increment a by one
    echo $a;
    a=`expr $a + 1`
done

#until command
b=0
until [ $b -lt 10 ]
do
    echo $b;
    b=`expr $b + 1`
done

# Nested loops

