# Shell Scripting

## Making files executable
We run <b>chmod +x filename.sh  </b> to make the file accept 
<b>./filename.sh </b>command.


## Getting started

### Echo command without new lines
<b>echo -n "sample text"</b> enforces text being printed on new line.

### Echo command with new lines
<b> echo "one" "two" "three"</b> script prints out the words in  a single line; Just like using multiple echo for each of the words without the newline character;

### String continuation character.
<b> echo "one" "two"  \ 
                        "three"</b> prints out the texts in a single line even with new line as the backslash acts as a string concat character in bash;

### Echo with tab characters
<b> echo -e "one\ttwo\tthree"</b> prints out with the tab in between the numbers where <b>\t</b> has been applied;

### Echo with newline characters
<b> echo -e "one\ntwo\nthree"</b> prints each on a separate line with use of -e and the newline character \n;


### Echo with command -x to display all the comman
Having <b>#!/bin/bash -x </b> as part of the file header prints out the progress of all commands we are executing in the bash script.

### Printing words having single quotes
A single quote inside the double quotes works perfectly. With the double quotes as part of the text , to make sure the double quotes are printed as part of the text we use backslash.
<b> echo -e "one two \"three\" four "</b>



### Writting to file
We use foward angle character to write to file
<b> echo -e "one two \"three\" four " > file.txt</b>

Having this command executed in succession means that the last input overwrites the initial saves text in the file;

In case we need to append text to whatever is inside a file we use double angle brackets as follows;

<b> echo -e "My favoutite SUV is volvo" >> file.txt</b>

### Comments
Single line comments we start the line with <b>#</b> character as follows.

<b># echo -e "one two \"three\" four " > file.txt</b> which comments out the whole line,

<b># echo -e "one two \"three\" four " # > file.txt</b> comments from where the hash character is at,

For multiple line we use 

```
<<LINUXHINT_MESSAGE
    text to be commented
LINUXHINT_MESSAGE
```

### Variables
We have three types of variables
> Local Variables - Variable availble at the instance of the shell
> Environmental Variables
> Shell Variables - Global variables required by the shell to run corectly
Defining Variables
We define a variable by using <b>VAR="variable sample"</b> ensuring no spaces in between the variable name, the equals sign and the variable itself. This is now can be used a part of the script execution as
<b>echo $VAR;</b>

For combining two variables prefferably of the same type we ...
```
NAME="sample" // scalar variable - can only hold one variable at a time
readonly NAME // ensured the value of readonly cannot be changed
unset NAME // removes the variable from the list of variables
COURSE= "code"
c="${a} ${b}"
echo "${c}";
```
Special Variables
```
#$0 // the name of the script

$1......9 // Corresponds to the arguments the script was invoked with i.e the positon of the argument

$# // Returns the number of arguments supplied to a script

$* // returns all the arguments which are double quoted

$? // returns the arguments which are individually double quoted

$$ // returns the process number of the current shell in the shell script // process ID under which its being executed



```


which outputs the combination of the two variables;

### HOME Variable
Stores the credentials of our system.
VAR_PATH=$HOME
echo "$VAR_PATH"

ls "$VAR_PATH"

### USER Variable
Defines as 
VAR_PATH=$USER
Prints out the user of the system;


### HOSTNAME Variable
Defines as 
VAR_PATH=$HOSTNAME
Prints out the host of the system;

### Permissions
Usually the construct of permissions works as follows
abc def ghi filename date
where ABC are your permissions, DEF for the user group and GHI for everyone.
So when you type CHMOD , the three following numbers depict USER, GROUP then OTHERS then the filename.

```
0 = nothing
1 - = e
2 - = w
3 - 2 + 1 = w + e
4 = r
5 - 4 + 1  = r + w
6 - 4 + 2 - r + w
7 - 4 + 2 + 1 = r + w + e
```

### Basic Operators
> Arithmetic Operators
> Relational Operators
> Boolean Operators
> String Operators
> File Test Operators


### Shell Loops 
> While Loop
> For loop
> Until Loop
> Nexted Loops
> Infinite loops and loop control


### Functions in schell scripting


### Use Cases in schell scripting
Network monitoring example
```
#!/bin/bash

is_alive_ping()
{
    ping -c 1 $1 > /dev/null
    [ $? -eq 0 ] && echo Node with IP: $i is up.
}

for i in 192.168.2.{1..255}
do
is_alive_ping $i & disown
done
exit;
```


```
#! /bin/bash

DIR=$(pwd)
PID=/var/run/service.pid
JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre
NAME="Sample Service"
JAR_FILE=$DIR/target/service-1.0.jar
MAIN_CLASS=com.techpoa.sample.main.DaemonEntry

MIN_MEMORY=-Xms256m
MAX_MEMORY=-Xmx512m

# You can enable a security policy if you need it here
SECURITY_POLICY=

# Set to 1 to enable debugging
DEBUG=1
DEBUG_OUTPUT_FILE=/var/log/applications/service/service_info.txt
DEBUG_ERROR_FILE=/var/log/applications/service/service_error.txt

JSVC=/usr/bin/jsvc

# DO NOT EDIT BELOW THIS LINE

usage() {
	echo $"Usage: $0 {start|stop|restart} "
	return 0
}

start() {
    echo $"Starting the $NAME..."

    cd $DIR

    if [[ $DEBUG -eq 1 ]]; then
        $JSVC -cwd $DIR -debug -pidfile $PID -home $JAVA_HOME $SECURITY_POLICY -outfile $DEBUG_OUTPUT_FILE -errfile $DEBUG_ERROR_FILE $MIN_MEMORY $MAX_MEMORY -cp $JAR_FILE $MAIN_CLASS
    else
        $JSVC -cwd $DIR -pidfile $PID -home $JAVA_HOME $SECURITY_POLICY $MIN_MEMORY $MAX_MEMORY -cp $JAR_FILE $MAIN_CLASS
    fi

    # Check status of the application
    if [[ $? -eq 0 ]]; then
        echo $"$NAME Successfully STARTED"
        echo
        return 0
    else
        echo $"Failed to START $NAME"
        echo
        return 1
    fi
}

stop() {
    echo $"Stopping the $NAME..."

    cd $DIR

    if [[ $DEBUG -eq 1 ]]; then
        $JSVC -cwd $DIR -debug -stop -home $JAVA_HOME -pidfile $PID $SECURITY_POLICY -outfile $DEBUG_OUTPUT_FILE -errfile $DEBUG_ERROR_FILE $MIN_MEMORY $MAX_MEMORY -cp $JAR_FILE $MAIN_CLASS
    else
        $JSVC -cwd $DIR -stop -home $JAVA_HOME -pidfile $PID $SECURITY_POLICY $MIN_MEMORY $MAX_MEMORY -cp $JAR_FILE $MAIN_CLASS
    fi

    if [[ -e $PID ]]; then
        # Kill the process (so we are sure that it has stopped)
        KPID=`cat $PID`
        KPID1=$(($KPID - 1))
        kill -9 $KPID $KPID1
        rm -f $PID
    fi

    # Check status of the application
    if [[ $? -eq 0 ]]; then
        echo $"$NAME Successfully STOPPED"
        echo
        return 0
    else
        echo $"Failed to STOP $NAME"
        echo
        return 1
    fi
    echo
}

restart() {
    cd $DIR

    stop

    sleep 3

    if [[ -e $PID ]]; then
        # Kill the process (so we are sure that it has stopped)
        KPID=`cat $PID`
        KPID1=$(($KPID - 1))
        kill -9 $KPID $KPID1
        rm -f $PID
    fi

    sleep 2

    start

    # Check status of the application
    if [[ $? -eq 0 ]]; then
        echo $"$NAME Successfully RESTARTED"
        echo
        return 0
    else
        echo $"Failed to RESTART $NAME"
        echo
        return 1
    fi

    echo
}

case "$1" in
    start)
        start
    ;;
    stop)
        stop
    ;;
    restart)
        restart
    ;;
    *)
        echo $"****************************"
        echo $"   Welcome to the $NAME"
        echo $"   Usage: $0 {start|stop|restart}"
        echo $"*****************************"
       
        exit 1

esac

exit $?


```


## References

- [ ] [Shell Scripting Tutorial | Shell Scripting Crash Course | Linux Certification Training | Edureka](https://www.youtube.com/watch?v=GtovwKDemnI)
- [ ] [Bash Scripting Examples](https://www.youtube.com/watch?v=q2z-MRoNbgM&t=16464s)
- [ ] [The Shell Scripting Tutorial](https://www.shellscript.sh/)
